/**
 * Yannig et Julien
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

/*******************************************************/
/**                    CONSTANTES                     **/
/*******************************************************/

// -- Type of Building
var SITE_AVAILABLE = -1;
var BARRACKS_KNIGHT_ALLIES = 0;
var BARRACKS_KNIGHT_ENEMIES = 1;
var BARRACKS_ARCHER_ALLIES = 2;
var BARRACKS_ARCHER_ENEMIES = 3;
var BARRACKS_GIANT_ALLIES = 4;
var BARRACKS_GIANT_ENEMIES = 5;
var MINE_ALLIES = 6;
var MINE_ENEMIES = 7;
var TOWER_ALLIES = 8;
var TOWER_ENEMIES = 9;

// -- Type of Price of Units
var PRICE_KNIGHT = 80; // gold
var PRICE_ARCHER = 100; // gold
var PRICE_GIANT = 140; // gold

// -- Ally / Enemy
var IS_ALLIES = 0;
var IS_ENEMIES = 1;

// -- Number Max Of Building
var NBR_BARRACKS_KNIGHT = 1;
var NBR_BARRACKS_ARCHER = 1;
var NBR_BARRACKS_GIANT = 0;
var NBR_MINES = 2;
var NBR_TOWERS = 4;

// -- Max Level Of Building
var LVL_MINE = 3;

// -- Type of Actions
var BUILD_BARRACK_KNIGHT_ACTION = 0;
var TRAINING_KNIGHT_ACTION = 1;
var BUILD_BARRACK_ARCHER_ACTION = 2;
var TRAINING_ARCHER_ACTION = 3;
var BUILD_BARRACK_GIANT_ACTION = 4;
var TRAINING_GIANT_ACTION = 5;
var BUILD_MINE_ACTION = 6;
var UPGRADE_MINE_ACTION = 7;
var BUILD_TOWER_ACTION = 8;
var UPGRADE_TOWER_ACTION = 9;
var TRAIN_ACTION = 10;
var MOVE_ACTION = 11;
var WAIT_ACTION = 12;

// -- Type of Scoring
var BUILD_BARRACK_KNIGHT_SCORE = 150;
var TRAINING_KNIGHT_SCORE = 80;
var BUILD_BARRACK_ARCHER_SCORE = 20;
var TRAINING_ARCHER_SCORE = 0;
var BUILD_BARRACK_GIANT_SCORE = 0;
var TRAINING_GIANT_SCORE = 0;
var BUILD_MINE_SCORE = 80;
var UPGRADE_MINE_SCORE = 80;
var BUILD_TOWER_SCORE = 20;
var UPGRADE_TOWER_SCORE = 25;
var TRAIN_SCORE = 50;
var MOVE_SCORE = 30;
var WAIT_SCORE = 10;

/*******************************************************/
/**                       OBJECTS                     **/

/*******************************************************/
function Site(idSite, x, y) {
    this.idSite = idSite;
    this.x = x;
    this.y = y;
    this.lvl = 0;
    this.maxLvl = 0;
    this.owner = -1;
    this.structureType = -1;
    this.touchedSite = false;
}

function Queen(owner, x, y, health) {
    this.owner = owner;
    this.x = x;
    this.y = y;
    this.health = health;
    this.isUnderAttack = false;
    this.starterX = -1;
    this.starterY = -1;
}

function Unit(owner, type, price) {
    this.owner = owner;
    this.type = type;   // KNIGHT, ARCHER, GIANT
    this.price = price; // 80 = KNIGHT, 100 = ARCHER, 140 = GIANT
}

/*******************************************************/
/**                       GLOBALS                     **/
/*******************************************************/
var firstLoop = true;
var golds = -1;
var sites = [];

/*******************************************************/
/**                       ACTIONS                     **/
/*******************************************************/
var tabValueFirstAction = [];
tabValueFirstAction[BUILD_BARRACK_KNIGHT_ACTION] = 0;
tabValueFirstAction[BUILD_BARRACK_ARCHER_ACTION] = 0;
tabValueFirstAction[BUILD_MINE_ACTION] = 0;
tabValueFirstAction[UPGRADE_MINE_ACTION] = 0;
tabValueFirstAction[BUILD_TOWER_ACTION] = 0;
tabValueFirstAction[UPGRADE_TOWER_ACTION] = 0;
tabValueFirstAction[MOVE_ACTION] = 0;
tabValueFirstAction[WAIT_ACTION] = 0;

var tabValueSecondAction = [];
tabValueSecondAction[TRAIN_ACTION] = 0;
tabValueSecondAction[TRAINING_KNIGHT_ACTION] = 0;
tabValueSecondAction[TRAINING_ARCHER_ACTION] = 0;
tabValueSecondAction[TRAINING_GIANT_ACTION] = 0;

/*******************************************************/
/**                   INITIALISATION                  **/
/*******************************************************/
// -- Sites
var numSites = parseInt(readline());
for (var i = 0; i < numSites; i++) {
    var inputs = readline().split(' ');
    var siteId = parseInt(inputs[0]);
    var x = parseInt(inputs[1]);
    var y = parseInt(inputs[2]);
    // var radius = parseInt(inputs[3])
    sites[siteId] = new Site(siteId, x, y);
}

// -- Queen
var queens = [];
queens[0] = new Queen(0, -1, -1, -1, 100);
queens[1] = new Queen(1, -1, -1, -1, 100);

// -- Debug Loop
var nbr_loop = 0;

/*******************************************************/
/**                        GAME                       **/
/*******************************************************/
while (true) {

    resetScoreTab();

    var inputs = readline().split(' ');
    golds = parseInt(inputs[0]);
    var touchedSite = parseInt(inputs[1]); // -1 if none

    // INITIALISATION DES ARRAYS
    var building = [];
    building[SITE_AVAILABLE] = [];
    building[BARRACKS_KNIGHT_ALLIES] = [];
    building[BARRACKS_KNIGHT_ENEMIES] = [];
    building[BARRACKS_ARCHER_ALLIES] = [];
    building[BARRACKS_ARCHER_ENEMIES] = [];
    building[BARRACKS_GIANT_ALLIES] = [];
    building[BARRACKS_GIANT_ENEMIES] = [];
    building[MINE_ALLIES] = [];
    building[MINE_ENEMIES] = [];
    building[TOWER_ALLIES] = [];
    building[TOWER_ENEMIES] = [];

    for (var i = 0; i < numSites; i++) {
        var inputs = readline().split(' ');
        var siteId = parseInt(inputs[0]);
        //var ignore1 = parseInt(inputs[1]);
        //var ignore2 = parseInt(inputs[2]);
        var structureType = parseInt(inputs[3]); // -1 = No structure, 2 = Barracks
        var owner = parseInt(inputs[4]); // -1 = No structure, 0 = Friendly, 1 = Enemy
        var param1 = parseInt(inputs[5]);
        var param2 = parseInt(inputs[6]);

        //sites[siteId].owner = owner;
        //sites[siteId].touchedSite = (siteId === touchedSite) ? true : false;

        if (structureType === -1) {
            building[SITE_AVAILABLE].push(sites[siteId]);
        } else if (structureType === 0) {
            sites[siteId].maxLvl = param1;
            building[(owner === 0) ? MINE_ALLIES : MINE_ENEMIES].push(sites[siteId]);
        } else if (structureType === 1) {
            building[(owner === 0) ? TOWER_ALLIES : TOWER_ENEMIES].push(sites[siteId]);
        } else if (structureType === 2) {
            if (param2 === 0) {
                building[(owner === 0) ? BARRACKS_KNIGHT_ALLIES : BARRACKS_KNIGHT_ENEMIES].push(sites[siteId]);
            } else if (param2 === 1) {
                building[(owner === 0) ? BARRACKS_ARCHER_ALLIES : BARRACKS_ARCHER_ENEMIES].push(sites[siteId]);
            } else if (param2 === 2) {
                building[(owner === 0) ? BARRACKS_GIANT_ALLIES : BARRACKS_GIANT_ENEMIES].push(sites[siteId]);
            }
        }
    }

    var numUnits = parseInt(readline());
    for (var i = 0; i < numUnits; i++) {
        var inputs = readline().split(' ');
        var owner = parseInt(inputs[2]); //0 = Friendly, 1 = Enemy
        var x = parseInt(inputs[0]);
        var y = parseInt(inputs[1]);
        var unitType = parseInt(inputs[3]); // -1 = QUEEN, 0 = KNIGHT, 1 = ARCHER
        var health = parseInt(inputs[4]);

        if (unitType === -1) {
            queens[owner].x = x;
            queens[owner].y = y;
            if (owner === IS_ALLIES && health < queens[IS_ALLIES].health) {
                queens[IS_ALLIES].isUnderAttack = true;
            }
            queens[owner].health = health;
            if (firstLoop) {
                queens[owner].starterX = x;
                queens[owner].starterY = y;
            }
        }
    }

    /*******************************************************/
    /**                 Values First Action               **/
    /*******************************************************/

    barracks_knight_management();
    barracks_archer_management();
    barracks_giant_management();
    mines_management();
    towers_management();

    /*******************************************************/
    /**                Values Second Action               **/
    /*******************************************************/

    knights_management();
    archers_management();
    giants_management();

    /*******************************************************/
    /**                       Rescue Action                  **/
    /*******************************************************/

    save_the_queen();

    var actions = getAction();
    doAction(actions);

    if (firstLoop) {
        firstLoop = false;
    }

    printErr("Numéro de loop : ", nbr_loop++);
}

/*******************************************************/
/**                    FUNCTIONS                      **/

/*******************************************************/

function getAction() {
    var firstAction = null;
    tabValueFirstAction.forEach(function (value, index) {
        if (firstAction === null || value > tabValueFirstAction[firstAction]) {
            firstAction = index;
        }
    });

    var secondAction = null;
    tabValueSecondAction.forEach(function (value, index) {
        if (secondAction === null || value > tabValueFirstAction[secondAction]) {
            secondAction = index;
        }
    });
    printErr(firstAction,secondAction)
    printErr(tabValueFirstAction[BUILD_TOWER_ACTION]);
    return [firstAction, secondAction];
}

function doAction(actions) {
    actions.forEach(function (action) {
        switch (action) {
            case BUILD_BARRACK_KNIGHT_ACTION:
                var idSite = getClosestSites(building[SITE_AVAILABLE], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' BARRACKS-KNIGHT');
                break;
            case TRAINING_KNIGHT_ACTION:
                if (golds > PRICE_KNIGHT && building[BARRACKS_KNIGHT_ALLIES].length > 0) {
                    print('TRAIN ' + getBuildBarraks(building[BARRACKS_KNIGHT_ALLIES], PRICE_KNIGHT));
                } else {
                    print('TRAIN');
                }
                break;
            case BUILD_BARRACK_ARCHER_ACTION:
                var idSite = getClosestSites(building[SITE_AVAILABLE], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' BARRACKS-ARCHER');
                break;
            case TRAINING_ARCHER_ACTION:
                if (golds > PRICE_ARCHER && building[BARRACKS_ARCHER_ALLIES].length > 0) {
                    print('TRAIN ' + getBuildBarraks(building[BARRACKS_ARCHER_ALLIES], PRICE_ARCHER));
                } else {
                    print('TRAIN');
                }
                break;
            case BUILD_BARRACK_GIANT_ACTION:
                var idSite = getClosestSites(building[SITE_AVAILABLE], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' BARRACKS-GIANT');
                break;
            case TRAINING_GIANT_ACTION:
                if (golds > PRICE_GIANT && building[BARRACKS_GIANT_ALLIES].length > 0) {
                    print('TRAIN ' + getBuildBarraks(building[BARRACKS_GIANT_ALLIES], PRICE_GIANT));
                } else {
                    print('TRAIN');
                }
                break;
            case BUILD_MINE_ACTION:
                var idSite = getClosestSites(building[SITE_AVAILABLE], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' MINE');
                break;
            case UPGRADE_MINE_ACTION:
                var idSite = getClosestSites(building[MINE_ALLIES], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' MINE');
                break;
            case BUILD_TOWER_ACTION:
            case UPGRADE_TOWER_ACTION:
                var idSite = getClosestSites(building[SITE_AVAILABLE], queens[0].starterX, queens[0].starterY);
                print('BUILD ' + idSite + ' TOWER');
                break;
            case MOVE_ACTION:
                print('MOVE ' + queens[0].starterX + ' ' + queens[0].starterY);
                break;
            case WAIT_ACTION:
                print('WAIT');
                break;
        }
    });
}

function barracks_knight_management() {
    if (building[BARRACKS_KNIGHT_ALLIES].length < NBR_BARRACKS_KNIGHT) {
        var diff_nbr = NBR_BARRACKS_KNIGHT - building[BARRACKS_KNIGHT_ALLIES].length;
        tabValueFirstAction[BUILD_BARRACK_KNIGHT_ACTION] += diff_nbr * BUILD_BARRACK_KNIGHT_SCORE;
    } else {
        building[BARRACKS_KNIGHT_ALLIES].forEach(function (barrack) {
            var maxBarracksTraining = Math.floor(golds / PRICE_KNIGHT);
            tabValueSecondAction[TRAINING_KNIGHT_ACTION] += maxBarracksTraining * TRAINING_KNIGHT_SCORE;
        });
    }
}

function barracks_archer_management() {
    if (building[BARRACKS_ARCHER_ALLIES].length < NBR_BARRACKS_ARCHER) {
        var diff_nbr = NBR_BARRACKS_ARCHER - building[BARRACKS_ARCHER_ALLIES].length;
        tabValueFirstAction[BUILD_BARRACK_ARCHER_ACTION] += diff_nbr * BUILD_BARRACK_ARCHER_SCORE;
    } else {
        building[BARRACKS_ARCHER_ALLIES].forEach(function (barrack) {
            var maxBarracksTraining = Math.floor(golds / PRICE_ARCHER);
            tabValueSecondAction[TRAINING_KNIGHT_ACTION] += maxBarracksTraining * TRAINING_ARCHER_SCORE;
        });
    }
}

function barracks_giant_management() {
    if (building[BARRACKS_GIANT_ALLIES].length < NBR_BARRACKS_GIANT) {
        var diff_nbr = NBR_BARRACKS_GIANT - building[BARRACKS_GIANT_ALLIES].length;
        tabValueFirstAction[BUILD_BARRACK_GIANT_ACTION] += diff_nbr * BUILD_BARRACK_GIANT_SCORE;
    } else {
        building[BARRACKS_GIANT_ALLIES].forEach(function (barrack) {
            var maxBarracksTraining = Math.floor(golds / PRICE_GIANT);
            tabValueSecondAction[TRAINING_KNIGHT_ACTION] += maxBarracksTraining * TRAINING_GIANT_SCORE;
        });
    }
}

function mines_management() {
    if (building[MINE_ALLIES].length < NBR_MINES) {
        var diff_nbr = NBR_MINES - building[MINE_ALLIES].length;
        tabValueFirstAction[BUILD_MINE_ACTION] += diff_nbr * BUILD_MINE_SCORE;
    } else {
        building[MINE_ALLIES].forEach(function (mine) {
            var diffLvlMine = LVL_MINE - mine.lvl;
            tabValueFirstAction[UPGRADE_MINE_ACTION] += diffLvlMine * UPGRADE_MINE_SCORE;
        });
    }
}

function towers_management() {
    if (building[TOWER_ALLIES].length < NBR_TOWERS) {
        var diff_nbr = NBR_TOWERS - building[TOWER_ALLIES].length;
        tabValueFirstAction[BUILD_TOWER_ACTION] += diff_nbr * BUILD_TOWER_SCORE;
    } else {
        building[TOWER_ALLIES].forEach(function (tower) {
            // Maybe upgrade ?
        });
    }
}

function knights_management() {
    if (building[BARRACKS_KNIGHT_ALLIES] > 0 && gold > PRICE_KNIGHT) {
        tabValueSecondAction[TRAINING_KNIGHT_ACTION] += TRAINING_KNIGHT_SCORE;
    }
}

function archers_management() {
    if (building[BARRACKS_ARCHER_ALLIES] > 0 && gold > PRICE_ARCHER) {
        tabValueSecondAction[TRAINING_ARCHER_ACTION] += TRAINING_ARCHER_SCORE;
    }
}

function giants_management() {
    if (building[BARRACKS_GIANT_ALLIES] > 0 && gold > PRICE_GIANT) {
        tabValueSecondAction[TRAINING_GIANT_ACTION] += TRAINING_GIANT_SCORE;
    }
}

function save_the_queen() {
    if (queens[IS_ALLIES].isUnderAttack) {
        tabValueFirstAction[BUILD_TOWER_ACTION] += 10000 * BUILD_TOWER_SCORE;
        queens[IS_ALLIES].isUnderAttack = false;
    }
}


function getClosestSites(sites, xPlayer, yPlayer) {
    var closestId = -1;
    var closestDistance = -1;

    if (sites.length === 0) {
        sites = building[SITE_AVAILABLE];
    }

    sites.forEach(function (site) {
        var newDistance = distance(site.x, site.y, xPlayer, yPlayer);
        if ((closestDistance === -1 || newDistance <= closestDistance)) {
            closestId = site.idSite;
            closestDistance = newDistance;
        }
    });

    return closestId;
}
function getClosestMine() {
    var closestId = -1;
    var closestDistance = -1;

    building[MINE_ALLIES].forEach(function (mine) {
        var newDistance = distance(mine.x, mine.y, queens[IS_ALLIES].x, queens[IS_ALLIES].y);
        if ((closestDistance === -1 || newDistance <= closestDistance) && mine.lvl < mine.maxLvl) {
            closestId = mine.idSite;
            closestDistance = newDistance;
        }
    });

    return closestId;
}



function getBuildBarraks(barracks, price_unit) {
    var maxBarracksTraining = Math.floor(golds / price_unit);
    var barracksTraining = [];

    barracks.forEach(function (barrack) {
        if (maxBarracksTraining > 0) {
            barracksTraining.push(barrack.idSite);
            maxBarracksTraining--;
        }
    });

    return barracksTraining.join(" ");
}

function distance(x1, y1, x2, y2) {
    return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}


function resetScoreTab() {
    tabValueFirstAction.forEach(function (value, index) {
        tabValueFirstAction[index] = 0;
    });

    tabValueSecondAction.forEach(function (value, index) {
        tabValueSecondAction[index] = 0;
    });
}