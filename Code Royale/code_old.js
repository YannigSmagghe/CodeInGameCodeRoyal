/**
 * Yannig et Julien
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

// CONSTANTES
// Actions
var TRAIN_ACTION = 1;
var BUILD_ACTION = 2;
var MOVE_ACTION = 3;

// Type of Barrack
var BARRACKS_KNIGHT = "BARRACKS-KNIGHT";
var BARRACKS_ARCHER = "BARRACKS-ARCHER";
var BARRACKS_GIANT = "BARRACKS-GIANT";
var MINE = "MINE";
var TOWER = "TOWER";

// Type of Units
var PRICE_KNIGHT = 80; // gold
var PRICE_ARCHER = 100; // gold
var PRICE_GIANT = 140; // gold

// Global
var BreakException = {};
var starter = true;
var starterX = -1;
var starterY = -1;
var lastBOB = -1;

// BUILDING
var nbrBarracksKnight = 1;
var nbrBarracksArcher = 0;
var nbrBarracksGiant = 0;
var nbrMine = 2;
var nbrTower = 0; // nombre de towers ? (calculer en fonction du nombre de site ?)

// ARMY
var nbKnight = 3;
var nbArcher= 0;
var nbGiant = 1;

// OBJECTS
var sites = [];
function Site(id, x, y, raduis, owner,type) {
	this.id = id;
	this.x = x;
	this.y = y;
	this.raduis = raduis;
	this.owner = owner;
	this.type = type;
}

var queens = [];
queens[0] = new Queen(0, -1, -1, -1, 100);
queens[1] = new Queen(1, -1, -1, -1, 100);

function Queen(owner, x, y, unitType, health) {
	this.owner = owner;
	this.x = x;
	this.y = y;
	this.unitType = unitType;
	this.health = health;
}

function Barraks(name, lvl) {
	this.name = name;
	this.lvl = lvl;
}

function Unit(name, price) {
	this.name = name;
	this.price = price;
}

// BUILD ORDER
var modBOB = 0;
var buildOrderBarraks = [
	new Barraks(MINE,3),
	new Barraks(BARRACKS_KNIGHT,1),
	new Barraks(MINE,3),
]

var modBOU = 0;
var buildOrderUnits = [
	new Unit(BARRACKS_KNIGHT,PRICE_KNIGHT),
];

// Game
var numSites = parseInt(readline());
for (var i = 0; i < numSites; i++) {
	var inputs = readline().split(' ');
	var siteId = parseInt(inputs[0]);
	sites[siteId] = new Site(parseInt(inputs[0]), parseInt(inputs[1]), parseInt(inputs[2]), parseInt(inputs[3]), -1);
}

// game loop
while (true) {
	var inputs = readline().split(' ');
	var gold = parseInt(inputs[0]);
	var touchedSite = parseInt(inputs[1]); // -1 if none
	for (var i = 0; i < numSites; i++) {
		var inputs = readline().split(' ');
		var siteId = parseInt(inputs[0]);
		var ignore1 = parseInt(inputs[1]);
		var ignore2 = parseInt(inputs[2]);
		var structureType = parseInt(inputs[3]); // -1 = No structure, 2 = Barracks
		var owner = parseInt(inputs[4]); // -1 = No structure, 0 = Friendly, 1 = Enemy
		var param1 = parseInt(inputs[5]);
		var param2 = parseInt(inputs[6]);

		sites[siteId].owner = owner;
	}

	var numUnits = parseInt(readline());
	for (var i = 0; i < numUnits; i++) {
		var inputs = readline().split(' ');
		var owner = parseInt(inputs[2]); //0 = Friendly, 1 = Enemy
		var x = parseInt(inputs[0]);
		var y = parseInt(inputs[1]);
		var unitType = parseInt(inputs[3]); // -1 = QUEEN, 0 = KNIGHT, 1 = ARCHER
		var health = parseInt(inputs[4]);

		if (unitType === -1) {
			queens[owner].x = x;
			queens[owner].y = y;
			queens[owner].health = health;

			if (owner === 0 && starter) {
				starterX = x; starterY = y; starter = false;
			}
		}
	}

    /*******************************************************/
	/**                     BUILDING                      **/
    /*******************************************************/

	// Construction des Barrack
	var siteClosest = -1;
	if(lastBOB === -1 || lastBOB !== buildOrderBarraks[modBOB].name) {
		siteClosest = getClosestSitesAvailable(starterX, starterY);
	}

	if (siteClosest !== -1) {
		// Si il reste des jetons pour construire le bat et que la queen est sur le site
		if (buildOrderBarraks[modBOB].name === BARRACKS_KNIGHT && buildOrderBarraks[modBOB].lvl > 0 && touchedSite === siteClosest && sites[siteClosest].owner === -1) {
			doAction(BUILD_ACTION, [siteClosest, BARRACKS_KNIGHT]);
			sites[siteClosest].type = BARRACKS_KNIGHT;
			buildOrderBarraks[modBOB].lvl--;
			if(buildOrderBarraks[modBOB].lvl === 0) { modBOB++; lastBOB = buildOrderBarraks[modBOB].name;}
		} else if ((buildOrderBarraks[modBOB].name === MINE && buildOrderBarraks[modBOB].lvl > 0 && touchedSite === siteClosest && sites[siteClosest].owner === -1) || sites[siteClosest].owner === 0) {
			doAction(BUILD_ACTION, [siteClosest, MINE]);
			sites[siteClosest].type = MINE;
			buildOrderBarraks[modBOB].lvl--;
			if(buildOrderBarraks[modBOB].lvl === 0) { modBOB++; lastBOB = buildOrderBarraks[modBOB].name;}
		} else if (buildOrderBarraks[modBOB].name === BARRACKS_ARCHER && buildOrderBarraks[modBOB].lvl > 0 && touchedSite === siteClosest && sites[siteClosest].owner === -1) {
			doAction(BUILD_ACTION, [siteClosest, BARRACKS_ARCHER]);
			sites[siteClosest].type = BARRACKS_ARCHER;
			buildOrderBarraks[modBOB].lvl--;
			if(buildOrderBarraks[modBOB].lvl === 0) { modBOB++; lastBOB = buildOrderBarraks[modBOB].name;}
		} else if (buildOrderBarraks[modBOB].name === BARRACKS_GIANT && buildOrderBarraks[modBOB].lvl > 0 && touchedSite === siteClosest && sites[siteClosest].owner === -1) {
			doAction(BUILD_ACTION, [siteClosest, BARRACKS_GIANT]);
			sites[siteClosest].type = BARRACKS_GIANT;
			buildOrderBarraks[modBOB].lvl--;
			if(buildOrderBarraks[modBOB].lvl === 0) { modBOB++; lastBOB = buildOrderBarraks[modBOB].name;}
		} else if (touchedSite === siteClosest && sites[siteClosest].owner === -1) {
			doAction(BUILD_ACTION, [siteClosest, TOWER]);
			sites[siteClosest].type = TOWER;
		} else {
			doAction(MOVE_ACTION, [sites[siteClosest].x, sites[siteClosest].y]);
		}
	} else {
		doAction(MOVE_ACTION, [starterX, starterY]);
	}

    /*******************************************************/
	/**                     TRAINING                      **/
    /*******************************************************/
	var training = false;
	try {
		sites.forEach(function (site) {
            /* Logique des poid */

            /* Construction dynamique */
			if (site.owner === 0 && site.type === buildOrderUnits[modBOU].name && gold > buildOrderUnits[modBOU].price) {
				doAction(TRAIN_ACTION, site.id);
				modBOU = (modBOU + 1) % buildOrderUnits.length;
				training = true;
				throw BreakException;
			}
		});
	}
	catch(e) { /* Nothing */}

	if(!training) {
		doAction(TRAIN_ACTION);
	}
}

// Function
function getClosestSitesAvailable(xPlayer, yPlayer) {
	var closestId = -1;
	var closestDistance = -1;

	sites.forEach(function (site) {
		var newDistance = distance(site.x, site.y, xPlayer, yPlayer);
		if ((closestDistance === -1 || newDistance <= closestDistance) && site.owner === -1) {
			closestId = site.id;
			closestDistance = newDistance;
		}
	});


	return closestId;
}

function distance(x1, y1, x2, y2) {
	return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}

function doAction(myAction, params = null) {
	switch (myAction) {
		case TRAIN_ACTION:
			if (params === null) {
				print('TRAIN');
			} else {
				print('TRAIN ' + params);
			}
			break;
		case BUILD_ACTION:
			print('BUILD ' + params[0] + ' ' + params[1]);
			break;
		case MOVE_ACTION:
			print('MOVE ' + params[0] + ' ' + params[1]);
			break;
		default:
			print('TRAIN');
	}

	lastAction = myAction;
}