/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

const MAP_MAX_HEIGHT = 1000;
const MAP_MIN_HEIGHT = 0;
const MAP_MAX_WIDTH = 1920;
const MAP_MIN_WIDTH = 0;

const QUEEN_MAX_MOVE_TURN = 60;

/** BUILD ORDER **/
var buildOrderList = [];

buildOrderList.push(new BuildOrder(2, 1)); // 0
buildOrderList.push(new BuildOrder(1, 1)); // 1


const BUILD_ORDER_SELECTED = 0;

/** BUILDING **/
const BARRACKS_TYPE_KNIGHT = 'KNIGHT';
const BARRACKS_TYPE_ARCHER = 'ARCHER';
var siteList = [];

/** UNIT **/

const UNIT_KNIGHT = 'KNIGHT';
const UNIT_ARCHER = 'ARCHER';
const BUILD_ORDER_UNIT = [UNIT_KNIGHT,UNIT_KNIGHT, UNIT_ARCHER];

var trainingUnitIndex = -1;
const KNIGHT_PRICE = 80;
const ARCHER_PRICE = 100;

var healthQueenAlly = 100;
var healthQueenEnnemy = 100;
var quuenStartPosition = {x:0,y:0};
var firstTurn = true;


var numSites = parseInt(readline());
for (var i = 0; i < numSites; i++) {
    var inputs = readline().split(' ');
    var siteId = parseInt(inputs[0]);
    var x = parseInt(inputs[1]);
    var y = parseInt(inputs[2]);
    var radius = parseInt(inputs[3]);
    siteList.push({id: siteList.length, site: new Site(siteId, x, y, radius)});
}

// game loop
while (true) {

    /** reset Tab **/
    var unitList = [];

    var inputs = readline().split(' ');
    var currentGold = parseInt(inputs[0]);
    var touchedSite = parseInt(inputs[1]); // -1 if none
    for (var i = 0; i < numSites; i++) {
        var inputs = readline().split(' ');
        var siteId = parseInt(inputs[0]);
        var ignore1 = parseInt(inputs[1]); // used in future leagues
        var ignore2 = parseInt(inputs[2]); // used in future leagues
        var structureType = parseInt(inputs[3]); // -1 = No structure, 2 = Barracks
        var owner = parseInt(inputs[4]); // -1 = No structure, 0 = Friendly, 1 = Enemy
        var param1 = parseInt(inputs[5]);
        var param2 = parseInt(inputs[6]);
        siteList[i].site.ignore1 = ignore1;
        siteList[i].site.ignore2 = ignore2;
        siteList[i].site.structureType = structureType;
        siteList[i].site.owner = owner;
        siteList[i].site.param1 = param1;
        siteList[i].site.param2 = param2;
    }
    var numUnits = parseInt(readline());
    for (var i = 0; i < numUnits; i++) {
        var inputs = readline().split(' ');
        var x = parseInt(inputs[0]);
        var y = parseInt(inputs[1]);
        var owner = parseInt(inputs[2]);
        var unitType = parseInt(inputs[3]); // -1 = QUEEN, 0 = KNIGHT, 1 = ARCHER
        var health = parseInt(inputs[4]);
        unitList.push({id: unitList.length, unit: new Unit(x, y, owner, unitType, health)});
    }
    var actionQueenType, moveX, moveY, siteId, barrackType = undefined;
    // Write an action using print()
    // To debug: printErr('Debug messages...');
    /** Init Turn**/
    var myQueen = getMyQueen();
    if (firstTurn){
        quuenStartPosition.x = myQueen.unit.x;
        quuenStartPosition.y = myQueen.unit.y;
    }

    var closestQueenSite = getClosestSiteId(myQueen.unit.x, myQueen.unit.y);
    actionMoveQueenClosestSite(closestQueenSite);
    // When we touch site we can build
    var barrackAvailable = buildNewBarrackAvailable(BUILD_ORDER_SELECTED);
    if (touchedSite === siteList[closestQueenSite].site.siteId && barrackAvailable.length > 0) {
        actionBuildQueen();
    }

    if (barrackAvailable.length === 0) {
        saveTheQueen();
    }

    // barrackBuiltOnMap();
    log('actionQueen', actionQueenType, moveX, moveY, touchedSite, barrackType)
    // First line: A valid queen action
    // Second line: A set of training instructions
    actionQueen(actionQueenType, moveX, moveY, touchedSite, barrackType);
    siteList[closestQueenSite].structureType = barrackType;
    // actionQueen(actionQueenType,moveX,moveY,siteId,barrackType);

    var siteIdTraining = manageTraining(BUILD_ORDER_UNIT);
    actionTrain(siteIdTraining);
    firstTurn = false;
}

function actionMoveQueenClosestSite(closestSite) {
    var site = siteList[closestSite];
    actionQueenType = 'MOVE';
    moveX = site.site.x;
    moveY = site.site.y;
}

function buildNewBarrackAvailable(BUILD_ORDER_SELECTED) {
    var barrackAvailable = [];
    if (buildOrderList[BUILD_ORDER_SELECTED].knight > 0) {
        barrackAvailable.push('knight')
    }
    if (buildOrderList[BUILD_ORDER_SELECTED].archer > 0) {
        barrackAvailable.push('archer')
    }
    return barrackAvailable;
}


function barrackBuiltOnMap() {
    log('barrack', siteList);
}

function actionBuildQueen() {
    actionQueenType = 'BUILD';
    if (buildOrderList[BUILD_ORDER_SELECTED].knight > 0) {
        barrackType = BARRACKS_TYPE_KNIGHT;
        buildOrderList[BUILD_ORDER_SELECTED].knight = buildOrderList[BUILD_ORDER_SELECTED].knight - 1;
    } else if (buildOrderList[BUILD_ORDER_SELECTED].archer > 0) {
        barrackType = BARRACKS_TYPE_ARCHER;
        buildOrderList[BUILD_ORDER_SELECTED].archer = buildOrderList[BUILD_ORDER_SELECTED].archer - 1;
    }
}

function saveTheQueen() {
    actionQueenType = 'MOVE';
    log('save', quuenStartPosition.x);
    moveX = quuenStartPosition.x;
    moveY = quuenStartPosition.y;
}

/**
 * Main action 1
 * @param actionQueenType
 * @param moveX
 * @param moveY
 * @param siteId
 * @param barrackType
 */
function actionQueen(actionQueenType, moveX, moveY, touchedSite, barrackType) {
    switch (actionQueenType) {
        case 'WAIT':
            return print('WAIT');
        case 'MOVE':
            return print('MOVE ' + moveX + ' ' + moveY);
        case 'BUILD':
            return print('BUILD ' + touchedSite + ' BARRACKS-' + barrackType);
    }
}

function manageTraining(buildOrderUnit) {
    // if current gold = prix de mon unité => produce et index ++
    var siteTrainingAvailable = [];
    var siteTraining = [];
    for (var site in siteList){
        if (siteList[site].structureType){
            siteTrainingAvailable.push(siteList[site])
        }
    }

    var nextUnit = '';
    // first round
    if (firstTurn){
        nextUnit = buildOrderUnit[0];
    }else{
        nextUnit = buildOrderUnit[trainingUnitIndex];
    }

    switch (nextUnit) {
        case UNIT_KNIGHT:
            if (currentGold >= KNIGHT_PRICE) {
                log('++', trainingUnitIndex);
                for (var site in siteTrainingAvailable){
                    if (siteTrainingAvailable[site].structureType === UNIT_KNIGHT){
                        siteTraining.push(siteTrainingAvailable[site].site.siteId);
                        trainingUnitIndex++;
                        break;
                    }
                }
                // check in array site training available si il en exite un a knight si oui push l'id et le retirer des sites available
            }
        case UNIT_ARCHER:
            if (currentGold >= ARCHER_PRICE) {
                for (var site in siteTrainingAvailable){
                    if (siteTrainingAvailable[site].structureType === UNIT_ARCHER){
                        siteTraining.push(siteTrainingAvailable[site].site.siteId);
                        trainingUnitIndex++;
                        break;
                    }
                }

            }
    }
    trainingUnitIndex = trainingUnitIndex >= buildOrderUnit.length ? 0 : trainingUnitIndex++;
    var str = siteTraining.toString();
    var res = str.replace(","," ");
    return res;
}

/**
 * Main action 2
 * @param siteId
 */
function actionTrain(siteIdTraining) {
    if (siteIdTraining){
        return print('TRAIN ' + siteIdTraining);
    }else{
        print('TRAIN')
    }

}

/**
 * Calculate distance between queen and all site
 * @param queenX
 * @param queenY
 */
function getClosestSiteId(queenX, queenY) {
    var distance = 100000;
    var indexSiteId = -1;
    var goToX = -1;
    var goToY = -1;
    for (var index in siteList) {
        var site = siteList[index];
        // If site is free to build
        if (site.site.structureType === -1 && site.site.owner !== 0) {
            var newDistance = distanceOperator(queenX, queenY, site.site.x, site.site.y);
            // if new distance is shortes than currentDistance
            if (distance > newDistance) {
                indexSiteId = site.site.siteId;
                distance = newDistance;
                goToX = site.site.x;
                goToY = site.site.y;
            }
        }
    }
    return indexSiteId;
}

function getMyQueen() {
    for (var index in unitList) {
        var unit = unitList[index];
        if (unit.unit.unitType === -1 && unit.unit.owner === 0) {
            return unit
        }
    }
    return undefined;
}

/**
 * @return {number}
 */
function distanceOperator(x1, y1, x2, y2) {
    return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}

/**
 *
 * @param siteId
 * @param x
 * @param y
 * @param radius
 * @constructor
 */
function Site(siteId, x, y, radius) {
    this.siteId = siteId;
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.ignore1 = ignore1
    this.ignore2 = ignore2
    this.structureType = structureType
    this.owner = owner
    this.param1 = param1
    this.param2 = param2
}

/**
 *
 * @param x
 * @param y
 * @param owner
 * @param unitType
 * @param health
 * @constructor
 */
function Unit(x, y, owner, unitType, health) {
    this.x = x;
    this.y = y;
    this.owner = owner;
    this.unitType = unitType;
    this.health = health;
}

function BuildOrder(knight, archer) {
    this.knight = knight;
    this.archer = archer;
}

function log(text, param) {
    if (Array.isArray(param)) {
        return printErr(text, ' : ', JSON.stringify(param));
    }
    if (typeof param === 'object') {
        return printErr(text, ' : ', JSON.stringify(param));
    }
    return printErr(text, ' : ', param)
}