/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/

var inputs = readline().split(' ');
var lightX = parseInt(inputs[0]); // the X position of the light of power
var lightY = parseInt(inputs[1]); // the Y position of the light of power
var initialTX = parseInt(inputs[2]); // Thor's starting X position
var initialTY = parseInt(inputs[3]); // Thor's starting Y position

/** PARAM */

var MAP_WIDTH =  40;
var MAP_HEIGHT = 18;
var thor_x = initialTX;
var thor_y = initialTY;


// game loop
while (true) {
    var remainingTurns = parseInt(readline()); // The remaining amount of turns Thor can move. Do not remove this line.

    // Write an action using print()
    // To debug: printErr('Debug messages...');
    // (31,4). Light position = (0,17).
    var move = '';
    if (thor_x > lightX && thor_y === lightY && thor_x-1 < MAP_WIDTH){
        move = 'W';
        thor_x--;
    }else if (thor_x < lightX && thor_y === lightY && thor_x+1 < MAP_WIDTH){
        move = 'E';
        thor_x++;
    }else if (thor_x === lightX && thor_y > lightY && thor_y-1 < MAP_HEIGHT){
        move = 'N'
        thor_y--;
    }else if (thor_x === lightX && thor_y < lightY  && thor_y+1 < MAP_HEIGHT){
        move = 'S'
        thor_y++;
    }
    else if (thor_x > lightX && thor_y > lightY && thor_x+1 < MAP_WIDTH && thor_y-1 < MAP_HEIGHT){
        move = 'NE'
        thor_x++;
        thor_y--;
    }else if (thor_x > lightX && thor_y < lightY && thor_x-1 < MAP_WIDTH && thor_y+1 < MAP_HEIGHT ){
        move = 'SW'
        thor_x--;
        thor_y++;
    }else if (thor_x < lightX && thor_y > lightY && thor_x-1 < MAP_WIDTH && thor_y-1 < MAP_HEIGHT){
        move = 'NW'
        thor_x--;
        thor_y--
    }else if (thor_x < lightX && thor_y < lightY && thor_x+1 < MAP_WIDTH && thor_y+1 < MAP_HEIGHT){
        move = 'SE'
        thor_x++;
        thor_y++;
    }

    printErr(thor_x,thor_y);
    print(move);
}