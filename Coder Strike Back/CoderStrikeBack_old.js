/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/



const radiusCheckPoint = 600;
const checkPointList = [];
const NUMBER_PLAYER = 2;
var currentTurn = 0;
var currentCheckPointIndex = 0;
var lastCheckPoint = {'x': 0, 'y': 0};
var lastCheckpointDist = 0;
var lastSpeed = 0;
var thrust = 100;
var lastThrust = 0;
var loopTurn = 0;
/**  PARAM */
var slowingAreaDist = 4000;
var slowingAreaThrust = 80;
var angle = 0;
var vx = 0;
var vy = 0;
var ra = 0;


function CheckPoint(x, y) {
    this.index = checkPointList.length;
    this.x = x;
    this.y = y;
    this.radius = radiusCheckPoint;
}

// game loop
while (true) {
    var inputs = readline().split(' ');
    var x = parseInt(inputs[0]);
    var y = parseInt(inputs[1]);
    var nextCheckpointX = parseInt(inputs[2]); // x position of the next check point
    var nextCheckpointY = parseInt(inputs[3]); // y position of the next check point
    var nextCheckpointDist = parseInt(inputs[4]); // distance to the next checkpoint
    var nextCheckpointAngle = parseInt(inputs[5]); // angle between your pod orientation and the direction of the next checkpoint
    var inputs = readline().split(' ');
    var opponentX = parseInt(inputs[0]);
    var opponentY = parseInt(inputs[1]);

    // Write an action using print()
    // To debug: printErr('Debug messages...');


    /** PLAN : calculer les 6 tours à venir et tracer la meilleure trajectoire
     * PS : BOOST Au départ pour éviter les collisions
     *
     * **/

    loopTurn+=NUMBER_PLAYER;

    /** Add first check point to list **/
    if (checkPointList.length === 0) {
        checkPointList.push(new CheckPoint(nextCheckpointX, nextCheckpointY));
    }
    /** Turn Count **/
    if (lastCheckPoint.x !== nextCheckpointX && lastCheckPoint.y !== nextCheckpointY) {
        currentTurn++;
        lastCheckPoint.x = nextCheckpointX;
        lastCheckPoint.y = nextCheckpointY;
    }

    registerCheckPoint(nextCheckpointX, nextCheckpointY);
    checkLastCheckPoint(nextCheckpointX, nextCheckpointY);
    var speed  = calculateSpeed(nextCheckpointDist,lastCheckpointDist);
    if (speed <= 100){
        if (thrust >= 10 && thrust < 50){
            thrust--;
        }else if (thrust > 50){
            thrust -= 5;
        }
    }
    if (speed === 0 ){
        thrust = 0;
    }
    if (nextCheckpointAngle <= 20 && checkAngle(nextCheckpointAngle) && speed <= 100){
        thrust = 100;
    }
    if (speed >= 100){
        thrust = 100;
    }
    if (nextCheckpointAngle <= 20 && checkAngle(nextCheckpointAngle) &&  nextCheckpointDist <= 2000){
        thrust = 100;
    }
    // if (nextCheckpointAngle <= 20 && checkAngle(nextCheckpointAngle) && nextCheckpointDist >= 6000){
    //     thrust='BOOST'
    // }
    //
    // if (thrust >= 80 && nextCheckpointDist <= slowingAreaDist ){
    //     printErr('Slowing Area')
    //     slowingArea();
    // }
    //First LAP
    // if (currentTurn <= 4) {
    //     // printErr('nextCheckpointAngle : ',nextCheckpointAngle)
    //     printErr('turn : ', currentTurn);
    printErr('angle : ', nextCheckpointAngle)
    printErr('thrust : ',thrust)
    printErr('nextCheckpointDist : ', nextCheckpointDist)
    //     printErr('nextCheckpointDist : ', nextCheckpointDist + ' ' + nextCheckpointX + ' ' + nextCheckpointY);
    //
    //     // var str = JSON.stringify(checkPointList);
    //     // printErr('checkpointlist', str);
    //     lastThrust = thrust;
    //     print(nextCheckpointX + ' ' + nextCheckpointY + ' ' + thrust);
    // }
    // // // LAP 2 & 3
    // if (currentTurn > 4) {
    //     currentCheckPointIndex = (currentTurn) % 4;
    //
    //     printErr('previous angle', str);
    //     printErr('nextCheckpointDist : ', nextCheckpointDist)
    //     printErr('angle : ', nextCheckpointAngle)
    //     printErr('thrust : ', thrust)
    //
    //     printErr(checkPointList[currentCheckPointIndex].x, checkPointList[currentCheckPointIndex].y);
    //     lastThrust = thrust;
    //
    //     print(nextCheckpointX + ' ' + nextCheckpointY + ' ' + thrust);
    // }
    print(nextCheckpointX + ' ' + nextCheckpointY + ' ' + thrust);

    printErr(loopTurn, speed);
    // You have to output the target position
    // followed by the power (0 <= thrust <= 100)
    // i.e.: "x y thrust"
    lastCheckpointDist = nextCheckpointDist;
    lastSpeed = speed;
}


function registerCheckPoint(nextCheckpointX, nextCheckpointY) {
    // Si x et y ne sont pas dans le tabeau c'est un nouveau checkpoint
    var newCheckPoint = true;
    checkPointList.forEach(function (checkPoint) {
        if (checkPoint.x === nextCheckpointX && checkPoint.y === nextCheckpointY) {
            newCheckPoint = false;
        }
    });
    if (newCheckPoint) {
        printErr('Add new checkpoint', nextCheckpointX, nextCheckpointY);
        checkPointList.push(new CheckPoint(nextCheckpointX, nextCheckpointY));
    }
}

function checkLastCheckPoint(nextCheckpointX,nextCheckpointY){
    if (lastCheckPoint.x !== nextCheckpointX && lastCheckPoint.y !== nextCheckpointY){
        lastCheckpointDist = nextCheckpointDist;
        // connerie
    }
}

function checkAngle(nextCheckpointAngle){
    if (nextCheckpointAngle <= 45 && nextCheckpointAngle >= 0){
        // printErr('function angle <= 45 and >=0')
        return true;
    }else{
        return false;
    }
}

function calculateSpeed(nextCheckpointDist,lastCheckpointDist) {
    return nextCheckpointDist - lastCheckpointDist;
}
/** PLUS TARD **/


/*******************************************************
 *************** CHECKPOINT GESTION ********************
 *******************************************************
 */


// printErr(nextCheckpointX,nextCheckpointY);
