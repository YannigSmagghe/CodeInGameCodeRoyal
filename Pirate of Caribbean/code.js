/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/


const SPEED_LOW = 0;
const SPEED_MEDIUM = 1;
const SPEED_HIGHT = 2;

const STOCK_RHUM = 0;

const MOVE_RIGHT = 0;
const MOVE_TOP_RIGHT = 1;
const MOVE_TOP_LEFT = 2;
const MOVE_LEFT = 3;
const MOVE_BOTTOM_LEFT = 4;
const MOVE_BOTTOM_RIGHT = 5;

const SHIP_ALLIES = 1;

const GRID_HIGHT = 21;
const GRID_WIDTH = 23;

let barrelCount = 0;
let barrelList = [];
let firstLoop = true;
let ship_1 = [];


// Print log
var log = (name, value) => {
    printErr(`${name}: ${value}`);
};

function Barrel(id, x, y) {
    this.id = id;
    this.x = x;
    this.y = y;
}

function MyShip(id, x, y, orientation, shipSpeed, group) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.orientation = orientation;
    this.shipSpeed = shipSpeed;
    this.group = group;
}

function Entity(id, x, y, orientation, shipSpeed, group) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.orientation = orientation;
    this.shipSpeed = shipSpeed;
    this.group = group;
}

function registerBarrel(entity) {
    // Si x et y ne sont pas dans le tabeau c'est un nouveau checkpoint
    var newBarrel = true;
    barrelList.forEach(function (barrel) {
        if (barrel.x === entity.x && barrel.y === entity.y) {
            newBarrel = false;
        }
    });
    if (newBarrel) {
        barrelList.push(new Barrel(entity.id, entity.x, entity.y));
    }
}

function getClosestBarrel(barrelList, ship) {
    var closestDistance = -1;
    var closestIndex = -1;
    var barrelClosest = 0;
    barrelList.forEach(function (barrel, key) {
        var newDistance = distance(barrel.x, barrel.y, ship.x, ship.y);
        if ((closestDistance === -1 || newDistance <= closestDistance)) {
            barrelClosest = barrel.id;
            closestIndex = key;
            closestDistance = newDistance;
        }
    });
    log('closest index',closestIndex);
    return closestIndex;
}




function distance(x1, y1, x2, y2) {
    return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}

// game loop
while (true) {

    let printReturn = '';
    var myShipCount = parseInt(readline()); // the number of remaining ships
    var entityCount = parseInt(readline()); // the number of entities (e.g. ships, mines or cannonballs)
    barrelList = [];
    for (var i = 0; i < entityCount; i++) {
        var inputs = readline().split(' ');
        var entityId = parseInt(inputs[0]);
        var entityType = inputs[1];
        var x = parseInt(inputs[2]);
        var y = parseInt(inputs[3]);
        var arg1 = parseInt(inputs[4]);
        var arg2 = parseInt(inputs[5]);
        var arg3 = parseInt(inputs[6]);
        var arg4 = parseInt(inputs[7]);
        if (entityType !== 'SHIP'
        // && firstLoop === true
        ) {
            var barrel = new Barrel(entityId, x, y);
            registerBarrel(barrel);
        }
        if (entityType === 'SHIP'
            && arg4 === SHIP_ALLIES
        ) {
            ship_1 = new MyShip(entityId, x, y, arg1, arg2, arg3, arg4);
        }
    }
    barrelCount = entityCount;
    // for(var i in BARREL_LIST)
    // {
    //     var value =JSON.stringify(BARREL_LIST[i]);
    log('my ship x', ship_1.x);
    log('my ship y', ship_1.y);
    log('barrelList', barrelList.length);
    // }

    var closestSiteId = getClosestBarrel(barrelList, ship_1);

    log('close id', closestSiteId);
    log('barrel x', barrelList[closestSiteId].x);
    log('barrel y',  barrelList[closestSiteId].y);

    for (var i = 0; i < myShipCount; i++) {

        // Write an action using print()
        // To debug: printErr('Debug messages...');
        print('MOVE ' + barrelList[closestSiteId].x + ' ' + barrelList[closestSiteId].y); // Any valid action, such as "WAIT" or "MOVE x y"

    }

    firstLoop = false;
}