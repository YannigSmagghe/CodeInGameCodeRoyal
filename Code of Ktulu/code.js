/**
 * Survive the wrath of Kutulu
 * Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)
 **/

var width = parseInt(readline());
var height = parseInt(readline());
var mapGrid = [];

for (var i = 0; i < height; i++) {
    var line = readline();
    mapGrid.push(line);
}
var inputs = readline().split(' ');
var sanityLossLonely = parseInt(inputs[0]); // how much sanity you lose every turn when alone, always 3 until wood 1
var sanityLossGroup = parseInt(inputs[1]); // how much sanity you lose every turn when near another player, always 1 until wood 1
var wandererSpawnTime = parseInt(inputs[2]); // how many turns the wanderer take to spawn, always 3 until wood 1
var wandererLifeTime = parseInt(inputs[3]); // how many turns the wanderer is on map after spawning, always 40 until wood 1


/** Group **/


// game loop
while (true) {
    /** local variable **/
    var explorerList = [];
    var ennemyList = [];


    var entityCount = parseInt(readline()); // the first given entity corresponds to your explorer
    for (var i = 0; i < entityCount; i++) {
        var inputs = readline().split(' ');
        var entityType = inputs[0];
        var id = parseInt(inputs[1]);
        var x = parseInt(inputs[2]);
        var y = parseInt(inputs[3]);
        var param0 = parseInt(inputs[4]);
        var param1 = parseInt(inputs[5]);
        var param2 = parseInt(inputs[6]);

        if (entityType === 'EXPLORER'){
            explorerList.push(new Explorer(id,x,y));
        }
        if (entityType === 'WANDERER'){
            explorerList.push(new Explorer(id,x,y));
        }
    }
    log('hein ? ',mapGrid);
    // #TO DO mettre à jour la grille avec les ennemy et les joueurs
    // Write an action using print()
    // To debug: printErr('Debug messages...');
    log('explorer',explorerList);
    var myExplorer = getMyExplorer();
    var explorerId = getClosestExplorer(myExplorer.x,myExplorer.y);
    log('go to ',myExplorer);
    log('follow this guys',explorerList[explorerId] );
    // tant que je suis isolé
    print('MOVE '+explorerList[explorerId].x + ' ' +explorerList[explorerId].y); // MOVE <x> <y> | WAIT
    // une fois que je suis plus isolé
    var coordSafeZone = getSafeZone();

}

function Explorer(id,x,y){
    this.id = id;
    this.x  = x;
    this.y  = y;
}

function getMyExplorer() {
    return explorerList[0];
}

/**
 * Calculate distance between 2 explorer
 * @param queenX
 * @param queenY
 */
function getClosestExplorer(myExplorerX, myExplorerY) {
    var distance = -1;
    var explorerId = -1;
    for (var index in explorerList) {
        if (index !== 0){
            var explorer = explorerList[index];
            // If site is free to build
                var newDistance = distanceOperator(myExplorerX, myExplorerY, explorer.x, explorer.y);
                if (newDistance > distance) {
                    explorerId = index;
                    distance = newDistance;
                }
        }
    }
    return explorerId;
}

function getSafeZone(myExplorerX,myExplorerY) {
    var freeSlot = getFreeSlot(myExplorerX,myExplorerY);
    return freeSlot;
}

function getFreeSlot(myExplorerX,myExplorerY){
    // check si c'est un espace vide avec mapgrid
    var coordList = [];
    //top+2 y-2
    coordList.push({x:myExplorerX,y : myExplorerY -2});
    //top+1 y-1
    coordList.push({x:myExplorerX,y : myExplorerY -1});
    //right+2 x+2
    coordList.push({x:myExplorerX + 2,y : myExplorerY});
    //right+1 x+1
    coordList.push({x:myExplorerX + 1,y : myExplorerY});
    //down+2 y + 2
    coordList.push({x:myExplorerX,y : myExplorerY + 2});
    //down+1 y + 1
    coordList.push({x:myExplorerX,y : myExplorerY + 1});
    //left+2 x-2
    coordList.push({x:myExplorerX - 2,y : myExplorerY});
    //left+1 x-1
    coordList.push({x:myExplorerX - 1,y : myExplorerY});

    return coordList;
}
/**
 * @return {number}
 */
function distanceOperator(x1, y1, x2, y2) {
    return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}

/**
 *
 * @param text
 * @param param
 * @returns {*}
 */
function log(text, param) {
    if (Array.isArray(param)) {
        return printErr(text, ' : ', JSON.stringify(param));
    }
    if (typeof param === 'object') {
        return printErr(text, ' : ', JSON.stringify(param));
    }
    return printErr(text, ' : ', param)
}